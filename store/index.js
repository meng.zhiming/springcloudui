import vue from 'vue'
import vuex from 'vuex'

vue.use(vuex)

const store = new vuex.Store({
  state: {
    token: sessionStorage.getItem('token'),
    permissions: sessionStorage.getItem('permissions')
  },
  mutations: {
    set_token (state, token) {
      state.token = token
      sessionStorage.setItem('token', token)
    },
    del_token (state) {
      state.token = ''
      sessionStorage.removeItem('token')
    },
    set_permissions(state, permissions) {
      state.permissions = permissions
      sessionStorage.setItem('permissions', permissions)
    },
    del_permissions (state) {
      state.permissions = []
      sessionStorage.removeItem('permissions')
    },
  }
})
export default store
