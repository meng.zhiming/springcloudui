let userPage1 = {
  totalCount: 13,
  start: 1,
  end: 10,
  pageNum: 1,
  list: [{
    id: 1,
    email: 'mzm@markit.com',
    userName: 'mzmMock',
    password: 'mzm@1234',
    status: true
  },
  {
    id: 2,
    email: 'hym@markit.com',
    userName: 'hymMock',
    password: 'hym@1234',
    status: true
  },
  {
    id: 3,
    email: 'ys3@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: false
  },
  {
    id: 4,
    email: 'ys4@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: true
  },
  {
    id: 5,
    email: 'ys5@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: true
  },
  {
    id: 6,
    email: 'ys6@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: true
  },
  {
    id: 7,
    email: 'ys7@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: true
  },
  {
    id: 8,
    email: 'ys8@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: true
  },
  {
    id: 9,
    email: 'ys9@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: false
  },
  {
    id: 10,
    email: 'ys10@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: true
  }]
}

export default userPage1
