let userPage2 = {
  totalCount: 13,
  start: 11,
  end: 20,
  pageNum: 2,
  list: [{
    id: 10,
    email: 'ys10@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: true
  },
  {
    id: 11,
    email: 'ys11@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: false
  },
  {
    id: 12,
    email: 'ys12@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: true
  },
  {
    id: 13,
    email: 'ys13@markit.com',
    userName: 'ysMock',
    password: 'ys@1234',
    status: false
  }]
}

export default userPage2
