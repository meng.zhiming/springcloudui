let token = {
  access_token: 'b9e5d498-9ae0-4635-a1f5-5952a53dd4e8',
  token_type: 'bearer',
  refresh_token: 'e147fe3e-284d-4d86-97ea-205931069729',
  expires_in: 299,
  scope: 'all'
}

let oauth = {
  token
}

export default oauth
