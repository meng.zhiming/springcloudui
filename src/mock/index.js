import Mock from 'mockjs'
import oauth from './oauth'
import userPage1 from './userPage1'
import userPage2 from './userPage2'

console.log('Mock OAuth')
Mock.mock('http://localhost:8080/oauth/token', oauth.token)
Mock.mock('http://localhost:9999/user/getUserList?start=1&size=10', userPage1)
Mock.mock('http://localhost:9999/user/getUserList?start=11&size=10', userPage2)
