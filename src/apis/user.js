import zuul from '../utils/zuul'
export function getUserList (start, size) {
  return zuul.get('/user/getUserList?' + 'start=' + start + '&size=' + size, {

  })
}

export function deleteUser (id) {
  return zuul.post('/user/deleteUser', {
    id: id
  })
}
