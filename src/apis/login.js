import oauth from '../utils/oauth'
import config from '../../config/config'
export function loginReq (username, password) {
  return oauth.post('/oauth/token', {
    grant_type: `${config.grant_type}`,
    username: username,
    password: password,
    scope: `${config.scope}`,
    client_id: `${config.clientId}`,
    client_secret: `${config.client_secret}`
  })
}

export function getPermission () {
  return oauth.get('/oauth/user', {

  })
}
