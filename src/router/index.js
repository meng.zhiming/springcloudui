import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/Home'
import Welcome from '@/components/Welcome'
import UserList from '@/components/UserList'
import UserDetail from '@/components/UserDetail'
import QuestionList from '@/components/QuestionList'
import store from '../../store/index'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {
        requireAuth: true
      },
      component: Home,
      redirect: '/home/welcome',
      children: [{
        path: '/home/userList',
        name: 'userList',
        component: UserList
      },
      {
        path: '/home/userDetail',
        name: 'userDetail',
        component: UserDetail
      },
      {
        path: '/home/questionList',
        name: 'questionList',
        component: QuestionList
      },
      {
        path: '/home/welcome',
        name: 'welcome',
        component: Welcome
      }]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(r => r.meta.requireAuth)) {
    console.log('token ' + store.state.token)
    if (store.state.token) {
      next()
    } else {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    }
  } else {
    next()
  }
})
export default router
