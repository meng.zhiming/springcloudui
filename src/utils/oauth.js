import axios from 'axios'
import Qs from 'Qs'
import config from '../../config/config'
import store from '../../store/index'

const service = axios.create({
  baseURL: `${config.oauthBaseUri}`,
  timeout: 5000,
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  transformRequest: [function (data) {
    return Qs.stringify(data)
  }],
  withCredentials: true
})

service.interceptors.request.use(config => {
  console.log('login request interceptors')
  if (store.state.token) {
    config.headers.Authorization = 'Bearer ' + store.state.token
  }
  return config
}, error => {
  return Promise.reject(error)
})

export default service
