import axios from 'axios'
import Qs from 'Qs'
import conf from '../../config/config'
import store from '../../store/index'
import router from '../router'

const service = axios.create({
  baseURL: `${conf.zuulBaseUri}`,
  timeout: 5000,
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  transformRequest: [function (data) {
    return Qs.stringify(data)
  }],
  withCredentials: true
})

// service.defaults.headers.common['Authorization'] = store.state.token

service.interceptors.request.use(config => {
  if (store.state.token) {
    config.headers.Authorization = 'bearer ' + store.state.token
  }
  return config
}, error => {
  return Promise.reject(error)
})

service.interceptors.response.use(response => {
  return response
}, error => {
  switch (error.response.status) {
    case 401:
      store.commit('del_token')
      store.commit('del_permissions')
      console.log('Token error, log out')
      router.replace({
        path: '/login',
        query: {redirect: router.currentRoute.fullPath}
      })
  }
})

export default service
