var config={
    logoutUri:"http://localhost:8081/login",
    localuri :"http://localhost:8081",
    redirect_uri : "http://localhost:8081/login",

    // OAuth2
    // Code
    oauthBaseUri:"http://localhost:8080",
    userAuthorizationUri:"localhost:8080/oauth/token",
    // Access_Token
    accessTokenUri :"localhost:8080/oauth/token",
    clientId: "client_1",
    client_secret:"123456",
    scope:"all",
    response_type:"token",
    grant_type : "password",
    code:"",

    // Zuul
    zuulBaseUri:"http://localhost:9999",

    // Permission
    UserRole: "ROLE_USER",
    AdminRole: "ROLE_ADMIN"
  }

  export default config;